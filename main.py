#!/usr/bin/python
#
# Copyright (c) 2011, Luke Snyder. This file is
# licensed under the Affero General Public License version 3 or later. See
# the COPYRIGHT file.

import sys
from time import sleep
wordList = []
args = None

def _appendWordToFile(word, fileLocation):
    fileObj = open(fileLocation, "a")
    fileObj.write(word+"\n")
    fileObj.close()

def createCombinations(charSet, desiredLen, inputText):
    compiledText = ""
    for char in charSet:
        compiledText = inputText + char
        if (len(compiledText) == desiredLen) and args.fileLocation:
            _appendWordToFile(compiledText, args.fileLocation)    
        if (len(compiledText) == desiredLen) and not args.noScreenOut:
            print compiledText
        if len(compiledText) < desiredLen:
            createCombinations(charSet, desiredLen, compiledText)
    return
    
if __name__ == "__main__":
	import argparse
	parser = argparse.ArgumentParser(description="A program to output possible combinations in a character set with a given length")
	parser.add_argument("-l", "--length", dest="length", type=int,
	                    help="length of combination strings", required=True)
	parser.add_argument("-c", "--charset", dest="charset", type=str, default="abcdefghijklnmopqrstuvwxyz",
	                    help="A list of valid characters")
	parser.add_argument("--file", type=str, dest="fileLocation",
	                    help="Location to output combinations")
	parser.add_argument("-r", "--resume", action="store_const",
	                    const=True, default=False, dest='resume',
	                    help="To resume a previous session")
	parser.add_argument("--no-screen", action="store_const", const=True, default=False, dest="noScreenOut",
						help="Dont print to the screen")
	parser.add_argument("--debug", action="store_const", const=True, default=False, dest='debug',
						help="Show some debug output")
	args = parser.parse_args()
	desiredLen = args.length
	if args.debug == True:
		charset = "abcd"
		possibleComb = len(charset)**desiredLen
		print sys.getrecursionlimit()
	else:
		charset = args.charset
		possibleComb = len(charset)**desiredLen
	sys.setrecursionlimit(len(charset)**desiredLen)
	print "Generating"
	print "There are %d possible combinations" % possibleComb
	createCombinations(charset, desiredLen , "")
	
